import random

nombres = ["Juan", "María", "Carlos", "Ana", "Luis", "Jose", "Pedro", "David", "Ángela"]
mascotas = ["Perro", "Gato", "Loro", "Hamster", "Tortuga", "PAto", "Cobaya", "Conejo", "Gato"]

nombre_elegido = random.choice(nombres)
mascota_elegida = random.choice(mascotas)

mensaje = f"¡Hola! Tu nombre es {nombre_elegido} y tu mascota ideal es {mascota_elegida}."

print(mensaje)
